/*
 *  @tmpl 首页模板
 */

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { XtnScroll } from '../../components/xtnScroll/Scroll'
import * as $ from 'jquery'
declare var wcPop: any
@Component({
    selector: 'app-index',
    template: `
        <xtn-scroll [IsRefreshFinish]="ScrollInfo.IsRefreshFinish" [IsNextPageFinish]="ScrollInfo.IsNextPageFinish" [IsShowTop]="true" [IsShowBottom]="false" (onRefresh)="onScrollRefresh()" (onNextPage)="onLoadNextPage()">
            <div class="wc__recordChat-list" id="J__recordChatList"> <ul class="clearfix">
                    <li class="flexbox flex-alignc wcim__material-cell isDing" (click)="routeUrl()"> <div class="img"><img src="../../assets/img/uimg/u__qun-angular.png" /></div>
                        <div class="info flex1">
                            <h2 class="title">Angular交流群</h2> <p class="desc clamp2">欢迎大家来到angular+ngrx学习阵营，分享交流项目实战经验</p>
                        </div>
                        <label class="time flex-selft">周五</label>
                    </li>
                    <li class="flexbox flex-alignc wcim__material-cell" (click)="routeUrl()"><div class="img"><img src="../../assets/img/uimg/u__qun-img01.png" /></div>
                        <div class="info flex1">
                            <h2 class="title">蛋蛋彩</h2> <p class="desc clamp2">今晚23:00准时开彩，请做好准备</p>
                        </div>
                        <label class="time flex-selft">22:30</label> <label class="topding"><i class="iconfont icon-pingbi"></i></label>
                    </li>
                    <li class="flexbox flex-alignc wcim__material-cell" (click)="routeUrl()"> <div class="img imgs">
                            <img src="../../assets/img/uimg/u__chat-img15.jpg" /><img src="../../assets/img/uimg/u__chat-img05.jpg" /><img src="../../assets/img/uimg/u__chat-img03.jpg" />
                            <img src="../../assets/img/uimg/u__chat-img14.jpg" /><img src="../../assets/img/uimg/u__chat-img02.jpg" /><img src="../../assets/img/uimg/u__chat-img06.jpg" />
                            <img src="../../assets/img/uimg/u__chat-img08.jpg" /><img src="../../assets/img/uimg/u__chat-img07.jpg" /><img src="../../assets/img/uimg/u__chat-img09.jpg" />
                        </div> <div class="info flex1">
                            <h2 class="title">王者荣耀开黑群</h2> <p class="desc clamp2">华北骑士:踢了吧，这种段位拉低档次，还怎么能一起上位。</p>
                        </div>
                        <label class="time flex-selft">14:17</label> <em class="wcim__badge">18</em>
                    </li> <li class="flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                        <div class="img"><img src="../../assets/img/uimg/u__chat-img07.jpg" /></div><div class="info flex1">
                            <h2 class="title">Wing ☺卍</h2>  <p class="desc clamp2">问题已经解决了，谢谢~</p>
                        </div>
                        <label class="time flex-selft">7月11日</label>
                    </li> </ul>
            </div>
        </xtn-scroll>
    `,
    styles: [``]
})
export class IndexComponent implements OnInit {
    constructor( private router: Router
    ) { }

    private ScrollInfo: any = {
        IsRefreshFinish: false, //是否下拉加载完成
        IsNextPageFinish: false, //是否上拉加载完成
    }
    onScrollRefresh(){
        this.ScrollInfo.IsRefreshFinish = false
        for (var i = 0; i < 10; i++) {
            // console.log(i);
        }

        setTimeout(() => {
            this.ScrollInfo.IsRefreshFinish = true
        }, 1000);
    }

    // 上拉加载更多
    onLoadNextPage(){
        this.ScrollInfo.IsNextPageFinish = false
        for (var i = 0; i < 10; i++) {
            // console.log(i);
        }

        // 隐藏加载提示
        setTimeout(() => {
            this.ScrollInfo.IsNextPageFinish = true
        }, 1000);
    }
    ngOnInit(){
        $("#J__recordChatList").on("contextmenu", "li", function(e){
            e.preventDefault();
            var _points = [e.clientX, e.clientY];
            var that = $(this);
            wcPop({
                skin: 'contextmenu', shade: true, shadeClose: true, opacity: 0, follow: _points,
                btns: [
                    {text: '标记为未读', style: 'font-size:14px;line-height:50px;'}, {text: '置顶聊天', style: 'font-size:14px;line-height:50px;'},
                    { text: '删除', style: 'font-size:14px;line-height:50px;',
                        onTap() { wcPop({
                                skin: 'android', content: '删除后，将会清空该聊天记录',
                                btns: [ {text: '取消', onTap() {wcPop.close();}},
                                    {text: '删除', style: 'color:#ff3b30', onTap() {
                                            that.remove(); wcPop.close();
                                        }
                                    }
                                ]
                            });
                        }
                    }
                ]
            });
        });
    }
    routeUrl(){
        this.router.navigate(['/chat/group-chat'])
    }
}
