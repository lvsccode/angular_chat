
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import * as $ from 'jquery'
declare var wcPop: any
@Component({
    selector: 'app-contact',
    template: `
        <!--//通讯录模块-->
        <div class="wc__addrFriend-list" id="J__addrFriendList">
            <ul class="clearfix rmt-25"> <li> <div class="group">
                        <div class="row flexbox flex-alignc wcim__material-cell" id="J__popScreen_newFriends">
                            <span class="icobg" style="background:#18cc48"><i class="iconfont icon-haoyou"></i></span><span class="name flex1">新的朋友</span>
                        </div><div class="row flexbox flex-alignc wcim__material-cell" id="J__popScreen_groupChat">
                            <span class="icobg" style="background:#378fe7"><i class="iconfont icon-peoples"></i></span><span class="name flex1">群聊</span>
                        </div>
                    </div>
                </li><li id="A">
                    <h2 class="initial">A</h2><div class="group">
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img02.jpg" /><span class="name flex1">Aster</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img01.jpg" /><span class="name flex1">Alibaba-粑粑</span>
                        </div>
                    </div>
                </li> <li id="B">
                    <h2 class="initial">B</h2> <div class="group">
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img09.jpg" /><span class="name flex1">马蓉 ▪ ☀☼㈱</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img03.jpg" /><span class="name flex1">布莱恩</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img10.jpg" /><span class="name flex1">Bear</span>
                        </div>
                    </div>
                </li> <li id="C">
                    <h2 class="initial">C</h2>
                    <div class="group">
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img15.jpg" /><span class="name flex1">CC_李嘉诚</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img05.jpg" /><span class="name flex1">雷军</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img04.jpg" /><span class="name flex1">张小龙</span>
                        </div>
                    </div>
                </li> <li id="D">
                    <h2 class="initial">D</h2><div class="group">
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">  <img class="uimg" src="../../assets/img/uimg/u__chat-img06.jpg" /><span class="name flex1">李彦宏-Robin</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img07.jpg" /><span class="name flex1">刘强东</span>
                        </div>
                    </div>
                </li>  <li id="E">
                    <h2 class="initial">E</h2>
                    <div class="group">  <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img08.jpg" /><span class="name flex1">Lm杨幂幂</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img11.jpg" /><span class="name flex1">Wqq_王巧巧</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img12.jpg" /><span class="name flex1">杨迪</span>
                        </div>
                    </div>
                </li> <li id="F">
                    <h2 class="initial">F</h2> <div class="group"> <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img13.jpg" /><span class="name flex1">wuli凡凡</span>
                        </div>
                        <div class="row flexbox flex-alignc wcim__material-cell" (click)="routeUrl()">
                            <img class="uimg" src="../../assets/img/uimg/u__chat-img14.jpg" /><span class="name flex1">Nice奶思</span>
                        </div>
                    </div>
                </li>
            </ul> <div class="total">15位联系人</div>
        </div>

        <div class="wcim__popup-tmpl">
            <div id="J__popupTmpl-newFriends" style="display:none">
                <div class="wcim__popupTmpl"> <div class="wcim__newFriends-panel"> <ul class="wcim__similarPanel-cells clearfix bg-fff rmar25" style="box-shadow:0 1px 1px #e9e9e9;border-radius:.2rem;"> <li><a class="flexbox flex-alignc" href="javascript:;"><span class="bg"><i class="iconfont icon-search c-9ea0a3 fs-36"></i></span><label class="flex1 flexbox"><input class="ipt-text flex1" type="text" placeholder="输入手机号/聊天室号搜索" /></label></a></li>
                        </ul>
                        <ul class="wcim__similarPanel-cells clearfix bg-fff rmar25" style="box-shadow:0 1px 1px #e9e9e9;border-radius:.2rem;">
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img06.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">Angle</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add">通过</button></label></a></li>
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img13.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">炳哥</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add on">已通过</button></label></a></li>
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img11.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">小布丁</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add wait">等待验证</button></label></a></li>
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img12.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">Bear</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add">通过</button></label></a></li>
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img15.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">阳光少年</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add on">已通过</button></label></a></li>
                            <li><a class="wcim__material-cell flexbox flex-alignc" href="javascript:;"><span class="avator"><img src="../../assets/img/uimg/u__chat-img14.jpg" /></span><label class="flex1 flexbox flex-alignc"><span class="flex1"><em class="db fs-30">杨迪</em><em class="db fs-24 c-9ea0a3 rmt-5">信息仅对好友可见</em></span> <button class="wc__btn-primary btn-add wait">等待验证</button></label></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles: [``]
})
export class ContactComponent implements OnInit {
    constructor(
        private router: Router
    ) { }
    ngOnInit(): void {
        $("#J__popScreen_newFriends").on("click", function(){
            var newfriendIdx = wcPop({
                id: 'wcim_fullscreen', skin: 'fullscreen', title: '好友验证', content: $("#J__popupTmpl-newFriends").html(),
                position: 'right',opacity: .1, xclose: true, style: 'background: #f5f7fb;',
                swipe: [ {direction: 'right', fn() {wcPop.close()}
                    }
                ]
            });
        });

        $("#J__addrFriendList li:not(:first-child)").on("contextmenu", ".row", function (e) {
            var _this = $(this);
            wcPop({
                skin: 'androidSheet', shadeClose: true,btns: [ {  text: '设置备注名及描述',style: 'line-height:50px;',
                    },
                    { text: '标为星标朋友',style: 'line-height:50px;',
                    },
                    { text: '加入黑名单',style: 'line-height:50px;',
                    },
                    { text: '删除',style: 'line-height:50px;',
                    }
                ]
            });
        });
    }

    // 链接跳转
    routeUrl(){
        this.router.navigate(['/contact/uinfo'])
    }
}
