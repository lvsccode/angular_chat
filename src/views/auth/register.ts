/*
 *  @tmpl 注册模板
 */

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
// 引入状态管理
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import * as actions from '../../ngrx/action'
declare var wcPop: any

import { getToken, checkTel } from '../../utils/common'
@Component({
    selector: 'app-register',
    template: `
        <div class="wcim__lgregWrapper flexbox flex__direction-column">
            <div class="wcim__lgregHeader flex1">
                <div class="slogan"><div class="logo"><img src="../../assets/img/logo.png" /></div><h2>Angualr-chatRoom聊天室</h2>
                </div>
                <div class="forms">
                    <form (ngSubmit)="handleSubmit()">
                        <ul class="clearfix">
                            <li class="flexbox flex-alignc"><i class="iconfont icon-shouji"></i><input class="iptxt flex1" [(ngModel)]="formField.tel" name="tel" type="tel" placeholder="请输入手机号" autoComplete="off" maxLength="11" /><em class="borLine"></em></li>
                            <li class="flexbox flex-alignc"><i class="iconfont icon-pass"></i><input class="iptxt flex1" [(ngModel)]="formField.pwd" name="pwd" type="password" placeholder="请输入密码" autoComplete="off" /><em class="borLine"></em></li>
                            <li class="flexbox flex-alignc"><i class="iconfont icon-vcode"></i><input class="iptxt flex1" [(ngModel)]="formField.vcode" name="vcode" type="text" placeholder="验证码" autoComplete="off" /><em class="borLine"></em><button class="btn-getcode" (click)="handleVcode()" [disabled]="formField.disabled">{{formField.vcodeText}}</button></li>
                        </ul>
                        <div class="btns"><button class="wc__btn-primary btn__login" type="submit">注册</button></div>
                        <div class="lgregLink align-c clearfix"> <a routerLink="/login">已有账号，去登录</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="wcim__lgregFooter"> <p class="version">Angular-chatRoom v1.0</p>
            </div>
        </div>
    `,
    styles: [``]
})
export class RegisterComponent implements OnInit {
    private formField = {
        tel: '', pwd: '', vcode: '',
        vcodeText: '获取验证码', disabled: false, time: 0
    }
    
    private auth: any
    constructor(
        private router: Router, private store: Store<{}>
    ) {
        let that = this
        this.store.select('auth').subscribe(v => {
            that.auth = v;
        })
    }

    ngOnInit(): void {
        if(this.auth.token){ this.router.navigate(['/index'])
        }
    }

    handleSubmit(){
        let that = this
        if(!this.formField.tel){wcPop({ content: '手机号不能为空！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else if(!checkTel(this.formField.tel)){ wcPop({ content: '手机号格式不正确！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else if(!this.formField.pwd){ wcPop({ content: '密码不能为空！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else if(!this.formField.vcode){ wcPop({ content: '验证码不能为空！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else{
            this.store.dispatch(new actions.setToken(getToken(64)))
            this.store.dispatch(new actions.setUser(this.formField.tel))
            wcPop({
                content: '注册成功！', style: 'background:#378fe7;color:#fff;', time: 2, shadeClose: false
            });
        }
    }

    // 60s倒计时
    handleVcode(){
        if(!this.formField.tel){wcPop({ content: '手机号不能为空！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else if(!checkTel(this.formField.tel)){ wcPop({ content: '手机号格式不正确！', style: 'background:#eb5a5c;color:#fff;', time: 2 });
        }else{
            this.formField.time = 60
            this.formField.disabled = true
            this.countDown()
        }
    }
    countDown(){
        if(this.formField.time > 0){
            this.formField.time--
            this.formField.vcodeText = '获取验证码('+this.formField.time+')'
            setTimeout(() => { this.countDown()
            }, 1000);
        }else{
            this.formField.time = 0
            this.formField.vcodeText = '获取验证码'
            this.formField.disabled = false
        }
    }
}
