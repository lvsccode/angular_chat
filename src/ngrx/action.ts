/*
 *  @desc // 现在我们已经创建了reducer，但是还没有对应的action来操作它们，所以接下来就来编写action
 */

import { Action } from '@ngrx/store';
import { defaultState } from './state';
export class setToken implements Action{
    readonly type = 'SET_TOKEN'
    constructor(public payload: any){window.sessionStorage.setItem('token', payload)
    }
}
export class setUser implements Action {
    readonly type = 'SET_USER'
    constructor(public payload: any) {window.sessionStorage.setItem('user', payload)
    }
}
export class logout implements Action {
    readonly type = 'SET_LOGOUT'
    constructor() { window.sessionStorage.removeItem('user')
        window.sessionStorage.removeItem('token')
    }
}
export type defaultAction = setToken | setUser | logout